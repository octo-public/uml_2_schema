import yaml


class Config:
    def __init__(self, config_path='../config/default_config.yaml', verbose=False):
        self.verbose = verbose
        self.config_path = config_path
        with open(config_path) as file:
            self.config = yaml.load(file, Loader=yaml.FullLoader)
            if self.verbose:
                print(self.config)

    def get_dict(self, dict_name):
        dictionary = self.config.get(dict_name)
        if self.verbose:
            print(list(dictionary))

        return dictionary


def configure(args=None, config_path='../../config/default_config.yaml'):
    """
    Warning: command line argument overrides config_path parameter

    :param args:
    :param config_path:
    :return:
    """

    if args:
        print("Program arguments:")
        for arg in args:
            print("Program argument: {}".format(arg))
        if len(args) > 1:
            config_path = args[1].split('=')[1]

    config = Config(config_path=config_path)
    return config



