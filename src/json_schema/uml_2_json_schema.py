import json
import sys
from enum import Enum

import xmljson
from xml.etree.ElementTree import fromstring

from config_loader import configure

namespace = "@{http://www.omg.org/spec/XMI/20131001}"
uml_namespace = "{http://www.eclipse.org/uml2/5.0.0/UML}"


class ProcessingMode(Enum):
    GATHER_CLASSES = 'gather_classes'
    GENERATE_DICT = 'generate_dict'


def print_(message):
    if config_dict.get('debug'):
        print(message)


def map_to_jsons_schema_type(field_type, namespace):
    type_mapping = {
        "uml:Class": "object",
        "uml:Package": ''
    }
    if field_type:
        field_type_metadata = None
        if namespace in field_type:
            strip_namespace = field_type.split(namespace)
            field_type = strip_namespace[1]
        if isinstance(field_type, dict):
            field_type_metadata = field_type.get(list(field_type.keys())[0]).get("meta")
            field_type = field_type.get(list(field_type.keys())[0]).get("type")
        json_schema_type = type_mapping.get(field_type)
        if json_schema_type or json_schema_type == '':
            return json_schema_type, None
        else:
            return field_type, field_type_metadata
    else:
        return '', None


def process_container(container, container_path, indent_count, data_types: dict, classes: dict, mode: ProcessingMode, config: dict, container_type:str = ''):
    indent_count += 1
    indent_string = indent_count * '    '
    print_(f"{indent_string}----> Processing: {container_path}")
    remove = False
    json_dict = {}
    container_name = container.get("@name")
    for field_key in container:
        print_(f"{indent_string}Found field: {container_path}/{field_key}")
        field = container.get(field_key)
        if isinstance(field, dict):
            field_name = field.get("@name")
            if field_name:
                print_(f"{indent_string}field_name: {field_name}")
            field_type = field.get(f"{namespace}type")
            if field_type:
                print_(f"{indent_string}field_type: {field_type}")
            if field_key == uml_namespace + 'Model':
                # root of the model
                if mode == ProcessingMode.GENERATE_DICT:
                    json_dict['$schema'] =  "http://json-schema.org/schema#"
                    json_dict['title'] = field_name
                    json_dict['description'] = field.get("ownedComment").get("body").get("$")
                    model_version = field_name[-3:]
                    json_dict['$id'] = f"http://thalesgroup.com/schemas/test-bench/v{model_version}/definitions.json"
                    json_dict['type'] = 'object'
                container_path = ""
            packages = field.get("packagedElement")
            if packages:
                packages_dict = {}
                for package in packages:
                    package_name = package.get("@name")
                    child_dict = process_container(package, container_path + "/" + package_name, indent_count, data_types=data_types, classes=classes, mode=mode, config=config)
                    if mode == ProcessingMode.GENERATE_DICT:
                        packages_dict.update(child_dict)
                if mode == ProcessingMode.GENERATE_DICT:
                    json_dict.update(packages_dict)
            elif field_key == "nestedClassifier":
                child_dict = process_container(field, container_path + "/" + field_name, indent_count, data_types=data_types, classes=classes, mode=mode, config=config)
                if mode == ProcessingMode.GENERATE_DICT:
                    if json_dict.get(container_name).get('properties').get(field_name):
                        if json_dict.get(container_name).get('properties').get(field_name).get("type") == "array":
                            json_dict.get(container_name).get('properties').get(field_name)['items'] = child_dict.get(field_name)
                        else:
                            json_dict.get(container_name).get('properties').update(child_dict)
                    else:
                        json_dict.get(container_name).get('properties').update(child_dict)

            elif field_key == "ownedAttribute":
                child_name = field.get("@name")
                print_(f"{indent_string}child_name: [{child_name}]")
                child_dict = process_container(field, container_path + "/" + child_name, indent_count, data_types=data_types, classes=classes, container_type=field_key, mode=mode, config=config)
                json_dict[container_name] = {"properties": child_dict}

            elif field_key == "packagedElement":
                child_name = field.get("@name")
                print_(f"{indent_string}child_name: [{child_name}]")
                child_dict = process_container(field, container_path + "/" + child_name, indent_count, data_types=data_types, classes=classes, container_type=field_key, mode=mode, config=config)
                json_dict[container_name] = {"properties": child_dict}

        elif isinstance(field, list):
            children_dict = {}
            for child in field:
                child_name = child.get("@name")
                if not child_name:
                    child_name = ''
                print_(f"{indent_string}child_name: [{child_name}]")
                child_dict = process_container(child, container_path + "/" + child_name, indent_count, data_types=data_types, classes=classes, container_type=field_key, mode=mode, config=config)
                if mode == ProcessingMode.GENERATE_DICT:
                    if child.get("upperValue") and child.get("upperValue").get("@value") != 1:
                        if not child_dict:
                            child_dict = {}
                        child_dict_keys = child_dict.get(child_name).keys()
                        child_dict_to_move_in_array = {}
                        for child_dict_key in child_dict_keys:
                            if not child_dict_key == "$comment":
                                child_dict_to_move_in_array.update({child_dict_key: child_dict.get(child_name).get(child_dict_key)})
                        child_dict_comment = child_dict.get(child_name).get("$comment")
                        child_dict = {child_name: {"type": "array", "items": child_dict_to_move_in_array}}
                        if child_dict_comment:
                            child_dict.get(child_name).update({"$comment": child_dict_comment})
                    children_dict.update(child_dict)
            if mode == ProcessingMode.GENERATE_DICT:
                if container.get(namespace + "type") == "uml:Package":
                    json_dict[container_name] = children_dict
                else:
                    if json_dict.get(container_name):
                        for child_dict_key in children_dict:
                            child_existing_in_json_dict = json_dict[container_name].get("properties").get(child_dict_key)
                            child_dict = children_dict.get(child_dict_key)
                            if child_existing_in_json_dict:
                                # If array, replace "$ref" of inner class with content
                                child_type = child_existing_in_json_dict.get("type")
                                if child_type == "array":

                                    json_dict[container_name].get("properties").get(child_dict_key)["items"] = child_dict
                                else:
                                    json_dict[container_name].get("properties").update({child_dict_key: child_dict})
                            else:
                                json_dict[container_name].get("properties").update({child_dict_key: child_dict})
                        # json_dict[container_name].get("properties").update(children_dict)
                    else:
                        json_dict[container_name] = {"properties": children_dict}
                    print_(f"{indent_string}container name: {container_name}")
        else:
            if field_key == namespace + "type":
                if field in ["uml:DataType"]:
                    # Simple data type
                    remove = True
                    field_id = container.get(namespace + "id")
                    data_types[field_id] = container.get("@name")
                    field_attribute = container.get('ownedAttribute')
                    if field_attribute:
                        if field_attribute.get('defaultValue'):
                            name = field_attribute.get('@name')
                            value = field_attribute.get('defaultValue').get('@value')
                            general_type = data_types.get(container.get('generalization').get("@general"))
                            data_types[field_id] = {container.get("@name"): {"type": general_type, "meta": {name: value}}}
                if field in ['uml:Class', 'uml:Enumeration']:
                    class_id = container.get(namespace + "id")
                    values = None
                    if field == 'uml:Enumeration':
                        values_dict = container.get('ownedLiteral')
                        if values_dict:
                            values = list()
                            for val_dict in values_dict:
                                values.append(val_dict.get('@name'))
                        remove = True

                    classes[class_id] = {container_path: {'type': field, 'values': values}}
                if field in ['uml:Association']:
                    remove = True
            print_(f"{indent_string}JSON schema proto candidate: {field_key} = {field} (remove={remove})")
            if remove:
                break
    field_comment = None
    field_comment_key = None
    field_enum = None
    field_default_value = None
    field_title = None
    if container_type == 'ownedAttribute':
        type_key = "@type"
        field_type = data_types.get(container.get(type_key))
        if not field_type:
            # Look in class definitions
            field_type_from_classes = classes.get(container.get(type_key))
            if field_type_from_classes:
                field_class_type = list(field_type_from_classes.keys())[0]
                if field_type_from_classes.get(field_class_type).get('type') == 'uml:Enumeration':
                    field_type = "string"
                    field_enum = field_type_from_classes.get(field_class_type).get('values')
                    field_default_value_dict = container.get('defaultValue')
                    if field_default_value_dict:
                        field_default_value = field_default_value_dict.get('@value')
                else:
                    field_type = "#" + field_class_type
        field_comment_el = container.get('ownedComment')
        if field_comment_el:
            field_comment_key = "$comment"
            field_comment = field_comment_el.get('body').get('$')
            print_(f'{indent_string}Field comment: {field_comment}')
    else:
        type_key = namespace + "type"
        field_type = container.get(type_key)
        field_comment_el = container.get('ownedComment')
        if field_comment_el:
            field_comment_key = "description"
            field_comment: str = field_comment_el.get('body').get('$')
            if config.get("custom_mapping"):
                if config.get("custom_mapping").get("object_title_keyword"):
                    mapping_container = config.get("custom_mapping").get("object_title_keyword").get("container_type")
                    if mapping_container == 'ownedComment':
                        mapping_pattern = config.get("custom_mapping").get("object_title_keyword").get("pattern")
                        if mapping_pattern + '=' in field_comment:
                            splits = field_comment.split(mapping_pattern + '=')
                            field_comment = splits[0].strip()
                            field_title = splits[1]
            print_(f'{indent_string}Field description: {field_comment}')

    field_name = container.get("@name")
    print_(f"{indent_string}JSON schema candidate: {field_name} = {field_type}")
    indent_count -= 1

    if mode == ProcessingMode.GENERATE_DICT:
        if not remove:
            field_type_json, meta = map_to_jsons_schema_type(field_type, namespace)
            field_type_name = 'type'
            if field_type_json and field_type_json[0] == '#':
                field_type_name = '$ref'
            elif field_type_json:
                if field_type == 'uml:Class' and container.get('generalization'):
                    field_type_name = 'allOf'
                    generalization_type = classes.get(container.get('generalization').get('@general'))
                    field_type_json = [{'$ref': "#" + list(generalization_type.keys())[0]}]
                    if json_dict.get(container_name):
                        props = json_dict.get(container_name).pop('properties')
                        if props:
                            field_type_json.append({'properties': props})
            if field_type_json != '' and json_dict.get(container_name):
                json_dict.get(container_name).update({field_type_name: field_type_json})
                if meta:
                    json_dict.get(container_name).update(meta)
            elif field_type_json != '' and container_name:
                json_dict[container_name] = {field_type_name: field_type_json}
                if meta:
                    json_dict.get(container_name).update(meta)

        if field_comment:
            json_dict.get(container_name).update({field_comment_key: field_comment})
        if field_enum:
            json_dict.get(container_name).update({'enum': field_enum})
        if field_default_value:
            json_dict.get(container_name).update({'default': field_default_value})
        if field_title:
            if not json_dict.get(container_name):
                json_dict['title'] = field_title
            else:
                json_dict.get(container_name).update({'title': field_title})
        return json_dict
    else:
        return None


if __name__ == '__main__':
    config_dict = configure(args=sys.argv).get_dict('uml_2_json_schema')
    input_path = config_dict.get('input_path')
    output_path = config_dict.get('output_path')

    with open(input_path, 'r') as xml_file:
        xml=xml_file.read()

    xml_obj = fromstring(text=xml)
    bf = xmljson.BadgerFish()
    json_obj = bf.data(root=xml_obj)
    json_text = json.dumps(json_obj, indent=2)

    container = json_obj
    data_types = {}
    classes = {}
    json_dict = process_container(container=container, container_path="", indent_count=0, data_types=data_types, classes=classes, mode=ProcessingMode.GATHER_CLASSES, config=config_dict)
    print_(f'data_types: {data_types}')
    print_(f'classes: {classes}')
    print_(f'json_dict: {json_dict}')
    json_dict: dict = process_container(container=container, container_path="", indent_count=0, data_types=data_types, classes=classes, mode=ProcessingMode.GENERATE_DICT, config=config_dict)
    print_(f'json_dict: {json_dict}')

    print(f'Writing JSON Schema to: {output_path}')
    with open(output_path, 'w') as json_file:
        json_file.write(json.dumps(json_dict, indent=2))

