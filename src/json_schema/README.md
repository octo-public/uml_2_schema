# UML to JSON Schema

## Description

JSON Schema generator from an UML model description.

Many features and target schemas could be added.

Contributions are welcome!

## Available generators
- JSON Schema (`.json` file) generator from UML 2.5 `.uml` file (`uml_2_json_schema.py`)
- CSV glossary generator from JSON Schema `.json` file (`json_schema_to_csv_glossary.py`)

## Supported Inputs

### **UML 2.0 Model**
  - **UML 2.0 .uml file** as produced by Eclipse UML tools (such as UMLDesigner)
  - **Supported features:** 
    - **Class Diagram** UML model
    - **Only two top level packages** are supported and must be called **"definitions"** and **"properties"**.
    They are generated as JSON schema top level elements with the same name   
    - **Upper value** of element cardinality higher different than 1 is generated as an array
    - **Generalization** are generated as a `$ref` inside an allOf element, along with the object properties
    - **Nested classifiers** are generated as embedded objects
    - **Schema identification** : version information is extracted from `@name` metadata of the UML Model using the last 4 characters and injected in 
    the `$id` root field of the generated JSON schema (to be enhanced, sure):
  
    ```
        @name = "model_name - vXXX" => version = "vXXX" => $id = <namespace>/<version>/definitions.json
    ```

## Supported Outputs

- JSON Schema `.json` file using `uml_2_json_schema`
- Glossary `.csv` files using `json_schema_to_csv_glossary`

## Configuration

### YAML centralized configuration
- The `default_config.yaml` file, located int the `config` folder, provides an example of the current configuration 
possibilities and structure
- You can use your own configuration files by starting from the `default_config.yaml` as a template. 
- If your configuration is generic (not confidential, not business-specific), please share it at the root of the `config` folder  
### Confidentiality
- To ensure confidentiality of configurations, two approaches may be used
    - **Outside the project**: you can place your configurations files outside the project (to ensure not to push it) and pass their path in the command line of the call to the generator
    - **Inside a subfolder of the `config` folder**: the `.gitignore` file is configured to ignore (and hence diable push) the contained subfolders

## Examples

See data/in for an example UML file input.

See data/out for the corresponding JSON schema output 

## Usage

- Clone / Download project
- Create a Python virtual environment (Conda or other)
- Install dependencies

## Technology

### Python environment

- Implemented under Python v3.7+. No backward compatibility achieved yet for earlier versions of Python  
