import json
import sys

import pandas as pd
from pandas import DataFrame

from config_loader import configure


def print_(message):
    if config_dict.get('debug'):
        print(message)


def get_field_type(ref: str, path_separator: str):
    # return ref.split("/")[-1]  # Get the last part of foreign_link
    return ref[2:].replace('/', path_separator)  # Get the last part of foreign_link


def process_properties(bo_type, field_name, properties_container, df, data_glossary_config: dict):
    path_separator = data_glossary_config.get("path_separator")
    field_properties = properties_container.get('properties')
    if field_properties:
        for subfield in field_properties.keys():
            field_description = ''
            foreign_link = ''
            values_string = ''
            subfield_type = ''
            subfield_object = field_properties.get(subfield)
            if isinstance(subfield_object, dict):
                if "$comment" in subfield_object.keys():
                    field_description = subfield_object.get('$comment')
                if "description" in subfield_object.keys():
                    field_description = subfield_object.get('description')

                allOf = subfield_object.get("allOf")
                if allOf:
                    for array_element in allOf:
                        if '$ref' in array_element.keys():
                            df = process_all_off(array_element, subfield, field_description, bo_type + path_separator + subfield, values_string, df, glossary_config=data_glossary_config)
                            subfield_type = get_field_type(array_element.get('$ref'), path_separator=path_separator)
                            foreign_link = subfield_type
                        else:
                            df = process_properties(bo_type=bo_type + path_separator + subfield, field_name=subfield, properties_container=array_element, df=df, data_glossary_config=data_glossary_config)
                else:
                    subfield_type = subfield_object.get('type')
                    if 'enum' in subfield_object.keys():
                        subfield_type = "enum"
                        values = subfield_object.get("enum")
                        values_string = ', '.join(values)
                        if 'default' in subfield_object.keys():
                            field_description = ' - '.join(filter(None, [field_description, 'Default=' + subfield_object.get('default')]))
                    if '$ref' in subfield_object.keys():
                        subfield_type = get_field_type(subfield_object.get('$ref'), path_separator=path_separator)
                        foreign_link = subfield_type
                    if subfield_type == 'object':
                        foreign_link = bo_type + path_separator + subfield
                        current_bo_type = bo_type + path_separator + subfield
                        subfield_type = foreign_link
                        df = process_properties(bo_type=current_bo_type, field_name=subfield, properties_container=subfield_object, df=df, data_glossary_config=data_glossary_config)
                        df_data_to_append = map_df_data({'object_path': current_bo_type, 'object_name': subfield, 'field_name': '', 'type': 'object',
                                                         'description': field_description, 'supported_values': '', 'value_example': values_string, 'foreign_link': ''},
                                                        glossary_config=data_glossary_config)

                        df = df.append(df_data_to_append, ignore_index=True)
                    if subfield_type == 'array':
                        subfield_array_items = subfield_object.get('items')
                        subfield_reference = subfield_array_items.get('$ref')
                        subfield_type_array_type = subfield_array_items.get('type')
                        if subfield_reference:
                            foreign_link = get_field_type(subfield_reference, path_separator=path_separator)
                            values_string = foreign_link
                            subfield_type += f'[{foreign_link}]'
                        elif subfield_type_array_type == 'object':
                            foreign_link = bo_type + path_separator + subfield
                            values_string = foreign_link
                            current_bo_type = bo_type + path_separator + subfield
                            subfield_type += f'[{foreign_link}]'
                            df = process_properties(bo_type=current_bo_type, field_name=subfield, properties_container=subfield_array_items, df=df, data_glossary_config=data_glossary_config)
                            df_data_to_append = map_df_data({'object_path': current_bo_type, 'object_name': subfield, 'field_name': '', 'type': 'object',
                                                             'description': field_description, 'supported_values': '', 'value_example': '', 'foreign_link': ''},
                                                            glossary_config=data_glossary_config)
                            df = df.append(df_data_to_append, ignore_index=True)
                        else:
                            subfield_type += f'[{subfield_type_array_type}]'

                    subfield_format = subfield_object.get('format')
                    if subfield_format:
                        field_description = ' - '.join(filter(None, [field_description, 'Format=\'' + subfield_format + '\'']))

            df_data_to_append = map_df_data({'object_path': bo_type, 'object_name': field_name, 'field_name': subfield, 'type': subfield_type,
                                     'description': field_description, 'supported_values': '', 'value_example': values_string, 'foreign_link': foreign_link},
                                     glossary_config=data_glossary_config)
            df = df.append(df_data_to_append, ignore_index=True)
    return df


def process_all_off(array_element, field_name, field_description, field_full_path, values_string, df, glossary_config):
    path_separator = glossary_config.get('path_separator')
    foreign_link = get_field_type(array_element.get('$ref'), path_separator=path_separator)
    subfield = ''
    subfield_type = foreign_link

    df_data_to_append = map_df_data({'object_path': field_full_path, 'object_name': field_name, 'field_name': subfield, 'type': subfield_type,
                                     'description': field_description, 'supported_values': '', 'value_example': values_string, 'foreign_link': foreign_link},
                                    glossary_config=glossary_config)
    df = df.append(other=df_data_to_append, ignore_index=True)
    return df


def map_df_data(values_dict: dict, glossary_config: dict):
    path_separator = glossary_config.get('path_separator')
    path_root = glossary_config.get('path_root')
    type_root = glossary_config.get('type_root')
    column_mapping = glossary_config.get('column_mapping')
    data = {}
    for key in values_dict.keys():
        __real_path_value = ''
        target_column_name = column_mapping.get(key)
        if target_column_name:
            data_value = values_dict.get(key)
            if key == 'object_path':
                __real_path_value = data_value
                if path_root != '':
                    data_value = path_root + path_separator + data_value
            if key == 'type' and type_root != '':
                data_value = type_root + path_separator + data_value
            current_value = data.get(target_column_name)
            if current_value:
                if data_value:
                    data.update({target_column_name: current_value + path_separator + data_value})
                else:
                    data.update({target_column_name: current_value})
            else:
                data.update({target_column_name: data_value})

            if key == 'object_path':
                data['__real_path_value'] = __real_path_value
    return data


def process_package(package, package_name, df: DataFrame, data_glossary_config: dict):
    path_separator = data_glossary_config.get("path_separator")
    for field_name in package.keys():
        print_(f'Processing: {field_name}')
        field = package.get(field_name)
        field_full_path = package_name + path_separator + field_name
        field_description = ''
        if "description" in field.keys():
            field_description = field.get('description')
        if "$comment" in field.keys():
            field_description = field.get('$comment')

        values_string = ''
        allOf = field.get("allOf")
        if allOf:
            for array_element in allOf:
                if '$ref' in array_element.keys():
                    df = process_all_off(array_element, field_name, field_description, field_full_path, values_string, df, glossary_config=data_glossary_config)
                else:
                    df = process_properties(bo_type=field_full_path, field_name=field_name, properties_container=array_element, df=df, data_glossary_config=data_glossary_config)
        elif field.get('properties'):
            df = process_properties(bo_type=field_full_path, field_name=field_name, properties_container=field, df=df, data_glossary_config=data_glossary_config)
        if field.get('type'):
            df_data_to_append = map_df_data({'object_path': field_full_path, 'object_name': field_name, 'field_name': '', 'type': field.get('type'),
                                             'description': field_description, 'supported_values': '', 'value_example': values_string, 'foreign_link': ''},
                                            glossary_config=data_glossary_config)
            df = df.append(other=df_data_to_append, ignore_index=True)

    return df


if __name__ == '__main__':
    config_dict = configure(args=sys.argv).get_dict('json_parser_to_csv_glossary')
    input_path = config_dict.get('input_path')

    f = open(input_path)
    data = json.load(f)
    f.close()

    # FIXME need for configuration for object type depending on the glossary target ? e.g. 'object' or 'Business Type'
    # FIXME add 'skip package' conf? Could produce collisions in Glossary!
    for data_glossary_config in config_dict.get('targets'):
        print(f"Generating CSV for : {data_glossary_config.get('name')}")
        output_path = data_glossary_config.get('output_path')
        column_names = data_glossary_config.get('column_names')
        df = pd.DataFrame({}, columns=column_names)
        column_mapping = data_glossary_config.get('column_mapping')
        definitions = data['definitions']
        df = process_package(definitions, 'definitions', df, data_glossary_config=data_glossary_config)
        properties = data['properties']
        df = process_package(properties, 'properties', df, data_glossary_config=data_glossary_config)
        # Create the hierarchical sorting index for the 'object_path' column
        path_separator = data_glossary_config.get('path_separator')
        df['__object_path_sorting_index'] = df[column_mapping.get('object_path')].apply(lambda p: sum(q == path_separator for q in p))
        sorting_columns = list(filter(None, ['__real_path_value', column_mapping.get('object_path'), '__object_path_sorting_index', column_mapping.get('object_name'), column_mapping.get('field_name'), column_mapping.get('type')]))
        df = df.sort_values(by=sorting_columns)
        df = df.drop(labels=['__object_path_sorting_index', '__real_path_value'], axis=1)
        print(f'Writing CSV to: {output_path}')
        df.to_csv(output_path, index=False)
