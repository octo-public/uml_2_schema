certifi==2021.10.8
numpy==1.21.5
pandas==1.3.5
python-dateutil==2.8.2
pytz==2021.3
PyYAML==6.0
six==1.16.0
xmljson==0.2.1
