# UML to Schema

This project aims to host generators of operational data schema files (for databases, data processing pipelines, ...)
from an UML model description.
It can also host generators for other types of operational files, such as CSV data glossaries.

Many features and target schemas could be added.

Contributions are welcome!

## Available Generators

### [UML to JSON Schema](https://gitlab.com/octo-public/uml_2_schema/-/tree/master/src/json_schema)
- JSON Schema (`.json` file) generator from UML 2.5 `.uml` file
- CSV glossary generator from JSON Schema `.json` file 

## Recommended Tooling

To design UML 2.5 models, the following tools are recommended:

- An UML 2.5 modeling environment in Eclipse such as :
  - [UMLDesigner](https://www.umldesigner.org/) (tested with [UML to JSON Schema](https://gitlab.com/octo-public/uml_2_schema/-/tree/master/src/json_schema))
  - [Papyrus](https://www.eclipse.org/papyrus/)
  - [Eclipse Modeling tools](http://www.eclipse.org/downloads/packages/eclipse-modeling-tools/oxygen2)

## Configuration

### YAML centralized configuration
  - Configuration of generators is centralized in the `config` folder and based on YAML files
  - See each generator README.md for specific details
### Confidentiality
  - To ensure confidentiality of configurations, two approaches may be used
    - **Outside the project**: you can place your configurations files outside the project (to ensure not to push it) and pass their path in the command line of the call to the generator
    - **Inside a subfolder of the `config` folder**: the `.gitignore` file is configured to ignore (and hence diable push) the contained subfolders

## Examples

The following UML 2.5 model is used as a demonstration of each generator features:
<img height="500" src="data/in/Demo%20Model%20Class%20Diagram.png" title="Demo Model" width="500"/>

The corresponding `.uml` file is: [`data/in/demo_model.uml` and is located here.](https://gitlab.com/octo-public/uml_2_schema/-/blob/master/data/in/demo_model.uml) 

See each generator documentation for the corresponding outputs.

## Typical Usage

- Clone / Download project
- Create a Python virtual environment (Conda or other)
- Install dependencies

## Technology

### Python environment

- Implemented under Python v3.7+. No backward compatibility achieved yet for earlier versions of Python
- One can use the `requirements.txt` file to install project dependencies
